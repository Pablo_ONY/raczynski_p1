package APP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;


public class app_manager {
	
	
	public ArrayList<Person> person=new ArrayList<Person>();
	
	
	public void add_elem( String imię,String nazwisko, String pesel, Integer wiek,Integer wzrost, Double depozyt, boolean wpłacony) {			
		
		Integer id=1;
		
		if(person.size()>0)
		 id=person.get(person.size()-1).getId()+1;
		
		
		person.add(new Person(id,imię,nazwisko,pesel,wiek,wzrost,depozyt,wpłacony));
	}
	
public void add_many( Integer many) {	
	

		
	ArrayList<String> name = new ArrayList<String>();
	try {
		FileInputStream fstream = new FileInputStream("imiona.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			name.add(strLine);
		}
		fstream.close();
	} catch (Exception e) {
	}
	
	
	ArrayList<String> surname = new ArrayList<String>();
	try {
		FileInputStream fstream = new FileInputStream("nazwiska.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			surname.add(strLine);
		}
		fstream.close();
	} catch (Exception e) {
	}
	
	ArrayList<Integer> height = new ArrayList<Integer>();
	try {
		FileInputStream fstream = new FileInputStream("wzrost.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			Integer temp=Integer.parseInt(strLine);
			height.add(temp);
		}
		fstream.close();
	} catch (Exception e) {
	}
	
	ArrayList<Double> money = new ArrayList<Double>();
	
	  Scanner scan;
	    File file = new File("depozyt.txt");
	    try {
	        scan = new Scanner(file);

	        while(scan.hasNextDouble())
	        {
	          
	            money.add(scan.nextDouble());
	        }

	    } catch (FileNotFoundException e1) {
	            e1.printStackTrace();
	    }
	
//	try {
//		FileInputStream fstream = new FileInputStream("depozyt.txt");
//		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
//		String strLine;
//		while ((strLine = br.readLine()) != null) {
//			Double temp=new Double(strLine);
//			
//			money.add(temp);
//		}
//		fstream.close();
//	} catch (Exception e) {
//	}
	
	ArrayList<String> pesele = new ArrayList<String>();
	try {
		FileInputStream fstream = new FileInputStream("pesele.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			pesele.add(strLine);
		}
		fstream.close();
	} catch (Exception e) {
	}
	
	ArrayList<Integer> age = new ArrayList<Integer>();
	try {
		FileInputStream fstream = new FileInputStream("wiek.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			Integer temp=Integer.parseInt(strLine);
			age.add(temp);
		}
		fstream.close();
	} catch (Exception e) {
	}
	
	int i=0;
	Random r=new Random();
	
	while(i<many) {
	
		Integer id=1;
		
		
		if(person.size()>0) 
		 id=person.get(person.size()-1).getId()+1;
		
		
	
		Person temp=new Person(id, name.get(r.nextInt(100)), surname.get(r.nextInt(100)), pesele.get(r.nextInt(100)),age.get(r.nextInt(100)), height.get(r.nextInt(100)), money.get(r.nextInt(100)), r.nextBoolean());
//		person.add(new Person(id, name.get(r.nextInt(101)), surname.get(r.nextInt(101)),pesele.get(r.nextInt(101)),age.get(r.nextInt(101)), height.get(r.nextInt(101)), money.get(r.nextInt(101)), r.nextBoolean()));
		person.add(temp);
		i++;

	}
		
	}

	
	public void delete(Integer id) {
	 int i=0;
		while(person.get(i).getId()!=id)
		i++;
	
		person.remove(person.get(i));
	}
	
	public void delete_all() {
	
		
			person.clear();
			
		}
			
		
	
	
	public  int search_bin(Integer search) {
		
//		ArrayList<Integer> id= new ArrayList<Integer>();
//		Integer n=0;
//		while(person.size()>n)
//	 id.add(person.get(n).getId());
//		
//		Collections.sort(id);
		
		int left = 0;
        int right = person.size() - 1;

        while (left <= right)
        {
            int currentPosition = left + ( right - left )/2;

            if (person.get(currentPosition).getId() == search)
            {
                return currentPosition;
            }

            if (person.get(currentPosition).getId() < search)
            {
                left = currentPosition + 1;
            }

            if (person.get(currentPosition).getId() > search)
            {
                right = currentPosition - 1;
            }
        }

        return -1;
		

		
		
	}
	
	
	public  int search_lin(Integer search) {
		
		Integer incr =0;
		
		do {
			
			
			if(person.get(incr).getId()==search)return search;
			
			incr++;
		}while(incr<person.size()) ;
		
		return -1;
	}
	
	
	
}
