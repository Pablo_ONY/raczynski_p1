package APP;

public class Person {

	public String getImię() {
		return imię;
	}



	public void setImię(String imię) {
		this.imię = imię;
	}



	public String getNazwisko() {
		return nazwisko;
	}



	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}



	public String getPesel() {
		return pesel;
	}



	public void setPesel(String pesel) {
		this.pesel = pesel;
	}



	public Integer getWiek() {
		return wiek;
	}



	public void setWiek(Integer wiek) {
		this.wiek = wiek;
	}



	public Double getDepozyt() {
		return depozyt;
	}



	public void setDepozyt(Double depozyt) {
		this.depozyt = depozyt;
	}



	public boolean isWpłacony() {
		return wpłacony;
	}



	public void setWpłacony(boolean wpłacony) {
		this.wpłacony = wpłacony;
	}


	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getWzrost() {
		return wzrost;
	}



	public void setWzrost(Integer wzrost) {
		this.wzrost = wzrost;
	}

	private Integer id;
	private String imię;
	private String nazwisko;
	private String pesel;
	private Integer wiek;
	private Double depozyt;
	private Integer wzrost;
	private boolean wpłacony;



	public Person(Integer id, String imię,String nazwisko, String pesel, Integer wiek,Integer wzorst, Double depozyt, boolean wpłacony) {
		this.id=id;
		this.imię=imię;
		this.nazwisko=nazwisko;
		this.pesel=pesel;
		this.wiek=wiek;
		this.depozyt=depozyt;
		this.wpłacony=wpłacony;	
		this.wzrost=wzorst;
		}
}
