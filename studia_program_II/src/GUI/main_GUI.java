package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import APP.Person;
import APP.app_manager;

import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.JComboBox;

public class main_GUI {

	private JFrame frame;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblWiek;
	private JLabel lblDepozyt;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_3;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTable table_1;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main_GUI window = new main_GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main_GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 1000);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		app_manager manager = new app_manager();

		JScrollPane scrollPane = new JScrollPane();

		JLabel lblImi = new JLabel("Imię");

		textField = new JTextField();
		textField.setColumns(10);

		JLabel lblNazwisko = new JLabel("Nazwisko");

		textField_1 = new JTextField();
		textField_1.setColumns(10);

		JLabel lblPesel = new JLabel("Pesel");

		textField_2 = new JTextField();
		textField_2.setColumns(10);

		lblWiek = new JLabel("Wzrost");

		JComboBox comboBox = new JComboBox();

		ArrayList<String> arr = new ArrayList<String>();
		try {
			FileInputStream fstream = new FileInputStream("wzrost.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				arr.add(strLine);
			}
			fstream.close();
		} catch (Exception e) {
		}

		String[] lines = arr.toArray(new String[arr.size()]);
		;

		comboBox.removeAllItems();

		for (String str : lines) {
			comboBox.addItem(str);
		}

		lblDepozyt = new JLabel("Depozyt");

		textField_4 = new JTextField();
		textField_4.setColumns(10);

		JLabel lblWzrost = new JLabel("Wiek");

		textField_5 = new JTextField();
		textField_5.setColumns(10);

		JCheckBox chckbxNewCheckBox = new JCheckBox("Wpłacony");

		JButton btnWprowad = new JButton("Zatwierdz");

		JTable table = new JTable();
		DefaultTableModel model = new DefaultTableModel();
		table.setModel(model);
		model.addColumn("ID");
		model.addColumn("IMIE");
		model.addColumn("NAZWISKO");
		model.addColumn("PESEL");
		model.addColumn("WIEK");
		model.addColumn("DEPOZYT");
		model.addColumn("WZROST");
		model.addColumn("ZAPłACONY");

		btnWprowad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				model.setRowCount(0);

				int temp = Integer.parseInt(comboBox.getSelectedItem().toString());

				double temp_2 = 0;
				try {

					temp_2 = Double.parseDouble(textField_4.getText().toString());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				boolean zapł = true;

				if (chckbxNewCheckBox.isSelected() == false)
					zapł = false;
				int wzrost = Integer.parseInt(textField_5.getText().toString());
				manager.add_elem(

						textField.getText(), textField_1.getText(), textField_2.getText(),wzrost, temp, temp_2, zapł);

				for (int n = 0; n < manager.person.size(); n++) {

					model.addRow(new Object[] { manager.person.get(n).getId(), manager.person.get(n).getImię(),
							manager.person.get(n).getNazwisko(), manager.person.get(n).getPesel(),
							manager.person.get(n).getWiek(), manager.person.get(n).getDepozyt(),
							manager.person.get(n).getWzrost(), manager.person.get(n).isWpłacony() });

				}

			}
		});

		JLabel lblUsun = new JLabel("Usun");

		textField_3 = new JTextField();
		textField_3.setColumns(10);

		JButton btnUsun = new JButton("Usun");
		
		btnUsun.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				
			Integer id = Integer.parseInt(textField_3.getText().toString());
				
				manager.delete(id);
				
				model.setRowCount(0);
				for (int n = 0; n < manager.person.size(); n++) {

					model.addRow(new Object[] { manager.person.get(n).getId(), manager.person.get(n).getImię(),
							manager.person.get(n).getNazwisko(), manager.person.get(n).getPesel(),
							manager.person.get(n).getWiek(), manager.person.get(n).getDepozyt(),
							manager.person.get(n).getWiek(), manager.person.get(n).isWpłacony() });

				}

			}
		});
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		
		JLabel lblIlePlGenerowa = new JLabel("Ile pól generować ?");
		
		JButton btnGeneruj = new JButton("Generuj");
		btnGeneruj.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
		Integer many=	Integer.parseInt(textField_6.getText().toString());
		manager.add_many(many);
		for (int n = 0; n < manager.person.size(); n++) {
		model.addRow(new Object[] { manager.person.get(n).getId(), manager.person.get(n).getImię(),
				manager.person.get(n).getNazwisko(), manager.person.get(n).getPesel(),
				manager.person.get(n).getWiek(), manager.person.get(n).getDepozyt(),
				manager.person.get(n).getWzrost(), manager.person.get(n).isWpłacony() });
		}
			}
		});
		
		JButton btnWyczyscListe = new JButton("Wyczysc liste");
		btnWyczyscListe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				model.setRowCount(0);
				manager.delete_all();
			}
		});
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		
		JButton btnSzukajB = new JButton("Szukaj B");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		JLabel lblCzasWykonania = new JLabel("Czas wykonania:");
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		
		JLabel label = new JLabel("Czas wykonania:");
		
		JButton btnSzukajA = new JButton("Szukaj A");
		

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(49)
					.addComponent(lblPesel)
					.addGap(112)
					.addComponent(lblWzrost, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(546, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(106)
							.addComponent(lblUsun))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(34)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(36)
									.addComponent(lblNazwisko))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(49)
									.addComponent(lblImi)))
							.addGap(107)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
									.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblWiek))
								.addComponent(lblDepozyt)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(50)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(53)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(lblIlePlGenerowa, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
										.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
										.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(btnWprowad)
										.addComponent(chckbxNewCheckBox)))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(92)
									.addComponent(btnUsun))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(80)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(btnGeneruj)
										.addComponent(btnWyczyscListe))))
							.addGap(71)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnSzukajA, GroupLayout.PREFERRED_SIZE, 474, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(textField_7, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(lblCzasWykonania, GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
									.addComponent(btnSzukajB, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
									.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 451, GroupLayout.PREFERRED_SIZE)
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(textField_8, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(label, GroupLayout.PREFERRED_SIZE, 346, GroupLayout.PREFERRED_SIZE))))))
					.addGap(50))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(60)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
					.addGap(7)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblImi)
								.addComponent(lblWiek))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblNazwisko)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(71)
							.addComponent(lblDepozyt)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPesel)
						.addComponent(lblWzrost))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(chckbxNewCheckBox)
							.addGap(18)
							.addComponent(btnWprowad)
							.addGap(40)
							.addComponent(lblUsun)
							.addGap(18)
							.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnUsun)
							.addGap(40)
							.addComponent(lblIlePlGenerowa))
						.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(textField_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblCzasWykonania)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(btnGeneruj)
							.addGap(40)
							.addComponent(btnWyczyscListe))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnSzukajB)
							.addGap(29)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnSzukajA)))
					.addGap(22))
		);
		
		table_1 = new JTable();
		DefaultTableModel models = new DefaultTableModel();
		table_1.setModel(models);
		models.addColumn("ID");
		models.addColumn("IMIE");
		models.addColumn("NAZWISKO");
		models.addColumn("PESEL");
		models.addColumn("WIEK");
		models.addColumn("DEPOZYT");
		models.addColumn("WZROST");
		models.addColumn("ZAPłACONY");
		scrollPane_1.setViewportView(table_1);
		
		
		btnSzukajB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Integer s= Integer.parseInt(textField_7.getText().toString());
				long startTime = System.nanoTime();
				Integer result=manager.search_bin(s);
				long endTime = System.nanoTime();
				long duration = (endTime - startTime);
			
				lblCzasWykonania.setText("Czas wykonania: "+duration+" nanosekund");
				
				models.setRowCount(0);
				if(result==-1)
					models.addRow(new Object[] { null, null,null,null,null,null,null,null});
				else
					models.addRow(new Object[] { manager.person.get(result).getId(), manager.person.get(result).getImię(),
							manager.person.get(result).getNazwisko(), manager.person.get(result).getPesel(),
							manager.person.get(result).getWiek(), manager.person.get(result).getDepozyt(),
							manager.person.get(result).getWzrost(), manager.person.get(result).isWpłacony() });
					
					
			}
		});
		
		btnSzukajA.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				Integer s= Integer.parseInt(textField_8.getText().toString());
				long startTime = System.nanoTime();
				Integer result=manager.search_lin(s);
				long endTime = System.nanoTime();
				long duration = (endTime - startTime);
			
				label.setText("Czas wykonania: "+duration+" nanosekund");
				
				models.setRowCount(0);
				if(result==-1)
					models.addRow(new Object[] { null, null,null,null,null,null,null,null});
				else
					models.addRow(new Object[] { manager.person.get(result-1).getId(), manager.person.get(result-1).getImię(),
							manager.person.get(result-1).getNazwisko(), manager.person.get(result-1).getPesel(),
							manager.person.get(result-1).getWiek(), manager.person.get(result-1).getDepozyt(),
							manager.person.get(result-1).getWzrost(), manager.person.get(result-1).isWpłacony() });
				
			}
		});

		table.getColumnModel().getColumn(0).setPreferredWidth(46);
		table.getColumnModel().getColumn(1).setPreferredWidth(57);
		table.getColumnModel().getColumn(3).setPreferredWidth(46);
		table.getColumnModel().getColumn(4).setPreferredWidth(47);
		scrollPane.setViewportView(table);
		frame.getContentPane().setLayout(groupLayout);
	}
}
